package controller;

import javafx.scene.control.*;
import repository.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.cell.PropertyValueFactory;
import dao.Person;
import repository.IRepository;
import utils.DataBases;

import java.util.List;

public class Controller {

    @FXML
    private TableView<Person> table;

    @FXML
    private TableColumn<Person, String> idColumn;

    @FXML
    private TableColumn<Person, String> firstNameColumn;

    @FXML
    private TableColumn<Person, String> lastNameColumn;

    @FXML
    private TableColumn<Person, String> ageColumn;

    @FXML
    private TableColumn<Person, String> cityColumn;

    @FXML
    private Button readButton;

    @FXML
    private TextField idTextField;

    @FXML
    private TextField firstNameTextField;

    @FXML
    private TextField lastNameTextField;

    @FXML
    private TextField ageTextField;

    @FXML
    private TextField cityTextField;

    @FXML
    private TextArea systemMessageTextArea;

    @FXML
    private ComboBox<String> databaseComboBox;

    private IRepository iRepository = new BinaryRepository();

    private final int DEFAULT_VALUE = -1;
    private final String CHECK_ID_MSG = "Please, check ID field! (It should contain numbers)";
    private final String CHECK_AGE_MSG = "Please, check Age field! (It should contain numbers)";

    @FXML
    void initialize() {
        prepareTable();

        ObservableList<String> databases = FXCollections.observableArrayList(DataBases.H2.toString(),
                DataBases.MySQL.toString(), DataBases.PostgreSQL.toString(),
                DataBases.Redis.toString(), DataBases.MongoDB.toString(),
                DataBases.Binary.toString(), DataBases.CSV.toString(),
                DataBases.XML.toString(), DataBases.YAML.toString(),
                DataBases.JSON.toString());
        databaseComboBox.setItems(databases);

        databaseComboBox.setOnAction(event -> {
            chooseComboBox(databaseComboBox);
        });
    }

    @FXML
    void handleCreateBtn(ActionEvent event) {
        String firstName = firstNameTextField.getText().trim();
        if (firstName.isEmpty()) {
            systemMessageTextArea.setText("Please, fill in FirstName");
            return;
        }

        String lastName = lastNameTextField.getText().trim();
        if (lastName.isEmpty()) {
            systemMessageTextArea.setText("Please, fill in LastName");
            return;
        }

        int age;
        try {
            age = Integer.parseInt(ageTextField.getText().trim());
        } catch (NumberFormatException e) {
            systemMessageTextArea.setText(CHECK_AGE_MSG);
            return;
        }

        String city = cityTextField.getText().trim();
        if (city.isEmpty()) {
            systemMessageTextArea.setText("Please, fill in City");
            return;
        }

        if (iRepository != null) {
            iRepository.createPerson(new Person(firstName, lastName, age, city));
            fillTable();
            systemMessageTextArea.setText("Person successfully created");
        } else {
            systemMessageTextArea.setText("You haven't choosed a database! Please, choose to continue working.");
            return;
        }

    }

    @FXML
    void handleReadBtn(ActionEvent event) {
        fillTable();
        readButton.setDisable(true);
    }

    @FXML
    void handleUpdateBtn(ActionEvent event) {
        String inputId = idTextField.getText().trim();
        if (inputId.isEmpty()){
            systemMessageTextArea.setText(CHECK_ID_MSG);
        }
        int id;
        try {
            id = Integer.parseInt(inputId);
        } catch (NumberFormatException e) {
            systemMessageTextArea.setText(CHECK_ID_MSG);
            return;
        }
        String firstName = firstNameTextField.getText().trim();
        if (firstName.isEmpty()) {
            systemMessageTextArea.setText("Please, fill in FirstName");
            return;
        }

        String lastName = lastNameTextField.getText().trim();
        if (lastName.isEmpty()) {
            systemMessageTextArea.setText("Please, fill in LastName");
            return;
        }

        int age;
        try {
            age = Integer.parseInt(ageTextField.getText().trim());
        } catch (NumberFormatException e) {
            systemMessageTextArea.setText(CHECK_AGE_MSG);
            return;
        }

        String city = cityTextField.getText().trim();
        if (city.isEmpty()) {
            systemMessageTextArea.setText("Please, fill in City");
            return;
        }
        iRepository.updatePerson(id, firstName, lastName, age, city);
        fillTable();
    }

    @FXML
    void handleDeleteBtn(ActionEvent event) {
        String id = idTextField.getText().trim();
        if (id.isEmpty()) {
            systemMessageTextArea.setText(CHECK_ID_MSG);
            return;
        }
        systemMessageTextArea.setText(iRepository.deletePerson("id", id));
        fillTable();
    }

    @FXML
    void deleteAllByAge(ActionEvent event) {
        String age = cityTextField.getText().trim();
        if (age.isEmpty()) {
            systemMessageTextArea.setText("Please, fill in Age");
            return;
        }
        systemMessageTextArea.setText(iRepository.deletePerson("age", age));
        fillTable();
    }

    @FXML
    void deleteAllByCity(ActionEvent event) {
        String city = cityTextField.getText().trim();
        if (city.isEmpty()) {
            systemMessageTextArea.setText("Please, fill in City");
            return;
        }
        systemMessageTextArea.setText(iRepository.deletePerson("city", cityTextField.getText().trim()));
        fillTable();

    }

    @FXML
    void deleteAllByFirstName(ActionEvent event) {
        String firstName = firstNameTextField.getText().trim();
        if (firstName.isEmpty()) {
            systemMessageTextArea.setText("Please, fill in FirstName");
            return;
        }
        systemMessageTextArea.setText(iRepository.deletePerson("firstName", firstNameTextField.getText().trim()));
        fillTable();
    }

    @FXML
    void deleteAllByLastName(ActionEvent event) {
        String lastName = lastNameTextField.getText().trim();
        if (lastName.isEmpty()) {
            systemMessageTextArea.setText("Please, fill in LastName");
            return;
        }
        systemMessageTextArea.setText(iRepository.deletePerson("lastName", lastName));
        fillTable();
    }

    @FXML
    void deleteAllPersonsBtn(ActionEvent event) {
        if (iRepository !=null) {
            systemMessageTextArea.setText(iRepository.deleteAll());
            fillTable();
        }else
            systemMessageTextArea.setText("You haven't choose a database! Please, choose");
            return;
    }

    @FXML
    void searchPersonByIDButton(ActionEvent event) {
        int id;
        try {
            id = Integer.parseInt(idTextField.getText().trim());
        } catch (NumberFormatException e) {
            systemMessageTextArea.setText(CHECK_ID_MSG);
            return;
        }
            fillTable(SearchCommands.findPersonByID(id, iRepository.readAll()));
            readButton.setDisable(false);
            systemMessageTextArea.setText("You see shown person who was found by ID");
    }

    @FXML
    void searchAllpersonsByAgeBtn(ActionEvent event) {

        int age;
        try {
            age = Integer.parseInt(ageTextField.getText().trim());
        } catch (NumberFormatException e) {
            systemMessageTextArea.setText(CHECK_AGE_MSG);
            return;
        }
        fillTable(SearchCommands.findPersonByAge(age, iRepository.readAll()));
        readButton.setDisable(false);
        systemMessageTextArea.setText("You see shown persons which were found by same Age");
    }

    @FXML
    void searchAllpersonsByCityBtn(ActionEvent event) {
        String city = cityTextField.getText().trim();
        if (city.isEmpty()) {
            systemMessageTextArea.setText("Please, fill in City");
            return;
        }
        fillTable(SearchCommands.findPersonByCity(city, iRepository.readAll()));
        readButton.setDisable(false);
        systemMessageTextArea.setText("You see shown persons which were found by same City");
    }

    @FXML
    void searchAllpersonsByFirstNameBtn(ActionEvent event) {
        String firstName = firstNameTextField.getText().trim();
        if (firstName.equals("")) {
            systemMessageTextArea.setText("Please, fill in FirstName");
            return;
        }
        fillTable(SearchCommands.findPersonByFirstName(firstName, iRepository.readAll()));
        readButton.setDisable(false);
        systemMessageTextArea.setText("You see shown persons which were found by same First name");

    }

    @FXML
    void searchAllpersonsByLastNameBtn(ActionEvent event) {
        String lastName = lastNameTextField.getText().trim();
        if (lastName.equals("")) {
            systemMessageTextArea.setText("Please, fill in LastName");
            return;
        }
        fillTable(SearchCommands.findPersonByLastName(lastNameTextField.getText(), iRepository.readAll()));
        readButton.setDisable(false);
        systemMessageTextArea.setText("You see shown persons which were found by same Last name");
    }


    private void chooseComboBox(ComboBox<String> comboBox) {
        switch (comboBox.getValue()) {
            case "Redis":
                iRepository = new RedisRepository();
                fillTable();
                break;
            case "MySQL":
                iRepository = new MySQLRepository();
                fillTable();
                break;
            case "MongoDB":
                iRepository = new MongoDBRepository();
                fillTable();
                break;
            case "H2":
                iRepository = new H2Repository();
                fillTable();
                break;
            case "PostgreSQL":
                iRepository = new PostgreRepository();
                fillTable();
                break;
            case "JSON":
                iRepository = new JSONRepository();
                fillTable();
                break;
            case "YAML":
                iRepository = new YAMLRepository();
                fillTable();
                break;
            case "CSV":
                iRepository = new CSVRepository();
                fillTable();
                break;
            case "XML":
                iRepository = new XMLRepository();
                fillTable();
                break;
            case "Binary":
                iRepository = new BinaryRepository();
                fillTable();
                break;
            default:
                systemMessageTextArea.setText("You haven't choose a database! Now you are using Binary.");
                fillTable();
        }
    }

    private void prepareTable() {
        idColumn.setCellValueFactory(new PropertyValueFactory<Person, String>("id"));
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<Person, String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<Person, String>("lastName"));
        ageColumn.setCellValueFactory(new PropertyValueFactory<Person, String>("age"));
        cityColumn.setCellValueFactory(new PropertyValueFactory<Person, String>("city"));
    }

    private void fillTable() {
        table.setItems(FXCollections.observableArrayList(iRepository.readAll()));
    }

    private void fillTable(List<Person> list) {
        table.setItems(FXCollections.observableArrayList(list));
    }

}
