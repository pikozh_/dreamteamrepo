package utils;

public enum DataBases {
    H2,
    MySQL,
    PostgreSQL,
    Redis,
    MongoDB,
    Binary,
    XML,
    YAML,
    CSV,
    JSON
}
