package repository;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.*;
import com.mongodb.util.JSON;
import dao.Person;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MongoDBRepository implements IRepository {

    private final String URL_PATH = "mongodb://heroku_dw5j1mr7:5i5v9hdptv6f9upk6v52sl7lg@ds353748.mlab.com:53748/heroku_dw5j1mr7";
    private DB db;
    final String SUCCESS_MESSAGE = "Delete was success!";
    final String FAILED_MESSAGE = "Person was not deleted!";

    private final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    public MongoDBRepository() {
        try {
            db = new MongoClient(new MongoClientURI(URL_PATH)).getDB("heroku_dw5j1mr7");
        } catch (Exception e) {
            db = null;
            System.out.println("Connection error!");
        }
    }

    public DB getDb() {
        if (db == null) return null;
        return db;
    }

    public DBCollection getDbCollection() {
        if (db == null) return null;
        return db.getCollection("users");
    }

    @Override
    public boolean createPerson(Person person) {
        person.setId(createID());
        DBObject documentPerson = (DBObject) JSON.parse(GSON.toJson(person));
        this.getDb().getCollection("users").insert(documentPerson);
        return true;
    }

    public int createID() {
        List<Person> persons = readAll();
        try {
            return persons.get(persons.size() - 1).getId() + 1;
        } catch (Exception e) {
            return persons.size() + 1;
        }
    }

    @Override
    public List<Person> readAll() {
        Iterator<DBObject> document = this.getDb().getCollection("users").find().iterator();
        var list = new ArrayList<Person>();
        boolean resBool = document.hasNext();
        while (document.hasNext()) {
            try {
                Person person = GSON.fromJson(document.next().toString(), Person.class);
                list.add(person);
            } catch (Exception e) {
                System.out.println("Deserialize fault an object: " + document.next().toString());
            }
        }

        return list;
    }

    @Override
    public String updatePerson(int id, String firstName, String lastName, int age, String city) {
        var query = "{\"id\":" + id + "}";
        DBObject documentPerson = (DBObject) JSON.parse(GSON.toJson(new Person(id, firstName, lastName, age, city)));
        DBCursor doc = this.getDbCollection().find((DBObject) JSON.parse(query));
        this.getDbCollection().update((DBObject) JSON.parse(query), documentPerson);
        return "Successfully updated!";
    }

    @Override
    public String deletePerson(String fieldName, String fieldValue) {

        if ((fieldName.equals("id")) | (fieldName.equals("age"))) {
            this.getDbCollection().remove(new BasicDBObject().append(fieldName, Integer.parseInt(fieldValue)));
            return SUCCESS_MESSAGE;
        } else if ((fieldName.equals("firstName")) | (fieldName.equals("lastName")) | (fieldName.equals("city"))) {
            this.getDbCollection().remove(new BasicDBObject().append(fieldName, fieldValue));
            return SUCCESS_MESSAGE;
        } else return FAILED_MESSAGE;
    }

    @Override
    public String deleteAll() {
        this.getDb().getCollection("users").drop();
        return "Delete was success!";
    }
}
