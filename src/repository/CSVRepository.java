package repository;


import dao.Person;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CSVRepository implements IRepository {

    //    public static ArrayList<Person> list = new ArrayList<>();
    static File csvOutputFile = new File("persons.csv");

    public boolean createPerson(Person person) {

        try {
            FileWriter fileWriter = new FileWriter(csvOutputFile, true);
            List<Person> personList = Arrays.asList(person);
            for (Person splitterPerson : personList) {
                fileWriter.write(toStringCSV(splitterPerson));
            }
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    public List<Person> readAll() {
        ArrayList<Person> list = new ArrayList<>();
        try {
            FileWriter fileWriter = new FileWriter(csvOutputFile, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            File file = new File("persons.csv");
            String line = (Files.readAllLines(file.toPath())).toString();
            String[] arrayPersons = line.split(",  ");
            for (String splitterPersons : arrayPersons) {

                if (splitterPersons.equals("]")) {
                    break;
                }
                if (splitterPersons.equals("[]")) {
                    break;
                }
                int id = Integer.parseInt(splitterPersons.substring(splitterPersons.indexOf("id") + 3,
                        splitterPersons.indexOf(",", splitterPersons.indexOf("id"))));

                String firstName = splitterPersons.substring(splitterPersons.indexOf("firstName") + 10,
                        splitterPersons.indexOf(",", splitterPersons.indexOf("firstName")));

                String lastName = splitterPersons.substring(splitterPersons.indexOf("lastName") + 9,
                        splitterPersons.indexOf(",", splitterPersons.indexOf("lastName")));

                int age = Integer.parseInt(splitterPersons.substring(splitterPersons.indexOf("age") + 4,
                        splitterPersons.indexOf(",", splitterPersons.indexOf("age"))));

                String city = splitterPersons.substring(splitterPersons.indexOf("city") + 5);
                city = city.replace("]", "");
                Person person = new Person(id, firstName, lastName, age, city);
                list.add(person);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    public String updatePerson(int id, String firstName, String lastName, int age, String city) {
        List<Person> personToUpdate = readAll();

        for (Person splitterPersonBeforeUpdate : personToUpdate) {
            if (splitterPersonBeforeUpdate.getId() == id) {
                splitterPersonBeforeUpdate.setFirstName(firstName);
                splitterPersonBeforeUpdate.setLastName(lastName);
                splitterPersonBeforeUpdate.setAge(age);
                splitterPersonBeforeUpdate.setCity(city);
            }
        }

        try {
            FileWriter writer = new FileWriter(csvOutputFile);

            StringBuilder result = new StringBuilder();

            for (Person splitterPersonAfterUpdate : personToUpdate) {
                result.append(toStringCSVForUpdate(splitterPersonAfterUpdate));
            }
            result = new StringBuilder(result.toString().replaceAll("]$", ""));
            writer.write(result.toString());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            return "error";
        }
        return "Person is updated.";
    }

    public String deletePerson(String fieldName, String fieldValue) {

        final String WRONG_MESSAGE = "Wrong entered id!";
        final String SUCCESS_MESSAGE = "Delete was success!";
        List<Person> personToDelete = readAll();

        switch (fieldName) {
            case "id":
                try {
                    int id = Integer.parseInt(fieldValue);
                    personToDelete.removeIf(person -> person.getId() == id);
                    FileWriter writer = new FileWriter(csvOutputFile);

                    StringBuilder result = new StringBuilder();

                    for (Person splitterPersonAfterDelete : personToDelete) {
                        result.append(toStringCSVForUpdate(splitterPersonAfterDelete));
                    }
                    result = new StringBuilder(result.toString().replaceAll("]$", ""));
                    writer.write(result.toString());
                    writer.close();
                    return SUCCESS_MESSAGE;
                } catch (NumberFormatException e) {
                    return WRONG_MESSAGE;
                } catch (IOException e) {
                    return "File not found!";
                }

            case "firstName":
                try {
                    personToDelete.removeIf(person -> person.getFirstName().equals(fieldValue));
                    FileWriter writer = new FileWriter(csvOutputFile);

                    StringBuilder result = new StringBuilder();

                    for (Person splitterPersonAfterDelete : personToDelete) {
                        result.append(toStringCSVForUpdate(splitterPersonAfterDelete));
                    }
                    result = new StringBuilder(result.toString().replaceAll("]$", ""));
                    writer.write(result.toString());
                    writer.close();
                    return SUCCESS_MESSAGE;
                } catch (IOException e) {
                    return "File not found!";
                }

            case "lastName":
                try {
                    personToDelete.removeIf(person -> person.getLastName().equals(fieldValue));
                    FileWriter writer = new FileWriter(csvOutputFile);

                    StringBuilder result = new StringBuilder();

                    for (Person splitterPersonAfterDelete : personToDelete) {
                        result.append(toStringCSVForUpdate(splitterPersonAfterDelete));
                    }
                    result = new StringBuilder(result.toString().replaceAll("]$", ""));
                    writer.write(result.toString());
                    writer.close();
                    return SUCCESS_MESSAGE;
                } catch (IOException e) {
                    return "File not found!";
                }

            case "age":
                try {
                    int age = Integer.parseInt(fieldValue);
                    personToDelete.removeIf(person -> person.getAge() == age);
                    FileWriter writer = new FileWriter(csvOutputFile);

                    StringBuilder result = new StringBuilder();

                    for (Person splitterPersonAfterDelete : personToDelete) {
                        result.append(toStringCSVForUpdate(splitterPersonAfterDelete));
                    }
                    result = new StringBuilder(result.toString().replaceAll("]$", ""));
                    writer.write(result.toString());
                    writer.close();
                    return SUCCESS_MESSAGE;
                } catch (NumberFormatException e) {
                    return WRONG_MESSAGE;
                } catch (IOException e) {
                    return "File not found!";
                }

            case "city":
                try {
                    personToDelete.removeIf(person -> person.getCity().equals(fieldValue));
                    FileWriter writer = new FileWriter(csvOutputFile);

                    StringBuilder result = new StringBuilder();

                    for (Person splitterPersonAfterDelete : personToDelete) {
                        result.append(toStringCSVForUpdate(splitterPersonAfterDelete));
                    }
                    result = new StringBuilder(result.toString().replaceAll("]$", ""));
                    writer.write(result.toString());
                    writer.close();
                    return SUCCESS_MESSAGE;
                } catch (IOException e) {
                    return "File not found!";
                }
            default:
                return "Enter value in ID-field!";
        }
    }

    public String deleteAll() {
        try {
            PrintWriter writer = new PrintWriter(csvOutputFile);
            writer.print("");
            writer.close();
        } catch (IOException e) {
            return "Error.";
        }
        return "All persons deleted.";
    }

    public String toStringCSV(Person person) {
        return
                " id=" + createID() +
                        ", firstName=" + person.getFirstName() +
                        ", lastName=" + person.getLastName() +
                        ", age=" + person.getAge() +
                        ", city=" + person.getCity() + "\n";
    }

    public String toStringCSVForUpdate(Person person) {
        return
                " id=" + person.getId() +
                        ", firstName=" + person.getFirstName() +
                        ", lastName=" + person.getLastName() +
                        ", age=" + person.getAge() +
                        ", city=" + person.getCity() + "\n";
    }


    public int createID() {
        List<Person> persons = readAll();
        try {
            return persons.get(persons.size() - 1).getId() + 1;
        } catch (Exception e) {
            return persons.size() + 1;
        }
    }

//    public String deletePerson(int id) {
//        ArrayList<Person> personToDelete = readAll();
//        personToDelete.removeIf(person -> person.getId() == id);
//        try {
//            FileWriter writer = new FileWriter(csvOutputFile);
//
//            String result = "";
//
//            for (Person splitterPersonAfterDelete : personToDelete) {
//                result += toStringCSV(splitterPersonAfterDelete);
//            }
//            result = result.replaceAll("]$", "");
//            writer.write(result);
//            writer.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }


//    public static String displayAll() {
//        ArrayList<Person> allPersons = readFromCSV();
//        StringBuilder displayPerson = new StringBuilder();
//        for (Person splitterPersons : allPersons) {
//            displayPerson.append(splitterPersons.toString());
//        }
//        return displayPerson.toString().replaceAll("]", "");
//
//    }
//


//    public static boolean checkId(int id) {
//        ArrayList<Person> personsList = readFromCSV();
//        for (Person splitterPersons : personsList) {
//            if (splitterPersons.getId() == id) {
//                return true;
//            }
//        }
//        return false;
//    }

//    public static ObservableList<Person> converterStringToObservableList(){
//        return FXCollections.observableArrayList(readFromCSV());
//    }


}
