package repository;


import dao.Person;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class JSONRepository implements IRepository {

    private static final String FILE_WAS_NOT_FOUND = "File was not found!";
    public static File fileJSON = new File("persons.json");

    public boolean createPerson(Person person) {
        try {

            FileWriter writer = new FileWriter(fileJSON, true);
            FileReader reader = new FileReader(fileJSON);

            BufferedReader bufferedReader = new BufferedReader(reader);

            if (reader.read() == -1) {
                String result = "[" + toStringJSON(person) + "]";
                writer.write(result);
                writer.close();
            } else {
                String result = bufferedReader.readLine();
                RandomAccessFile file = new RandomAccessFile(fileJSON, "rw");
                file.seek(result.lastIndexOf("]"));
                file.write("},".getBytes());
                file.write((toStringJSON(person) + "]").getBytes());
                file.close();
                bufferedReader.close();
                writer.close();
            }
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    public List<Person> readAll() {
        ArrayList<Person> listOfPersons = new ArrayList<>();
        try {
            FileWriter fileWriter = new FileWriter(fileJSON, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileJSON));
            String line = bufferedReader.readLine();
            if (line == null) {
                return listOfPersons;
            }
            String[] arrayPersons = line.split("},");
            for (String splitterPersons : arrayPersons) {
                if (splitterPersons.equals("[]")) {
                    break;
                }
                int id = Integer.parseInt(splitterPersons.substring(splitterPersons.indexOf("id") + 6,
                        splitterPersons.indexOf(",")));
                String firstName = splitterPersons.substring(splitterPersons.indexOf("firstName") + 14,
                        splitterPersons.indexOf("\",", splitterPersons.indexOf("firstName")));
                String lastName = splitterPersons.substring(splitterPersons.indexOf("lastName") + 13,
                        splitterPersons.indexOf("\",", splitterPersons.indexOf("lastName")));
                int age = Integer.parseInt(splitterPersons.substring(splitterPersons.indexOf("age") + 7,
                        splitterPersons.indexOf(",", splitterPersons.indexOf("age"))));
                String city = splitterPersons.substring(splitterPersons.indexOf("city") + 9,
                        splitterPersons.indexOf("\"", splitterPersons.indexOf("city") + 9));
                Person person = new Person(id, firstName, lastName, age, city);
                listOfPersons.add(person);
                bufferedReader.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listOfPersons;
    }

    public String updatePerson(int id, String firstName, String lastName, int age, String city) {
        List<Person> personToUpdate = readAll();

        for (Person splitterPersonBeforeUpdate : personToUpdate) {
            if (splitterPersonBeforeUpdate.getId() == id) {
                splitterPersonBeforeUpdate.setFirstName(firstName);
                splitterPersonBeforeUpdate.setLastName(lastName);
                splitterPersonBeforeUpdate.setAge(age);
                splitterPersonBeforeUpdate.setCity(city);
            }
        }
        try {
            FileWriter writer = new FileWriter(fileJSON);

            StringBuilder result = new StringBuilder("[");

            for (Person splitterPersonAfterUpdate : personToUpdate) {
                result.append(toStringJSONForUpdate(splitterPersonAfterUpdate)).append(",");
            }
            result = new StringBuilder(result.toString().replaceAll(",$", "]"));
            writer.write(result.toString());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "The changes were successful!";
    }

    public String deletePerson(String fieldName, String fieldValue) {
        final String WRONG_MESSAGE = "Wrong id was entered!";
        final String SUCCESS_MESSAGE = "Delete was successful!";
        List<Person> personToDelete = readAll();

        switch (fieldName) {
            case "id":
                try {
                    int id = Integer.parseInt(fieldValue);
                    personToDelete.removeIf(person -> person.getId() == id);
                    FileWriter writer = new FileWriter(fileJSON);
                    StringBuilder result = new StringBuilder();
                    for (Person splitterPersonAfterDelete : personToDelete) {
                        result.append(toStringJSONForUpdate(splitterPersonAfterDelete)).append(",");
                    }
                    result = new StringBuilder(result.toString().replaceAll(",$", "]"));
                    if (result.toString().equals("[")) {
                        result = new StringBuilder();
                    }
                    writer.write(result.toString());
                    writer.close();
                    return SUCCESS_MESSAGE;
                } catch (NumberFormatException e) {
                    return WRONG_MESSAGE;
                } catch (IOException e) {
                    return FILE_WAS_NOT_FOUND;
                }

            case "firstName":
                try {
                    personToDelete.removeIf(person -> person.getFirstName().equals(fieldValue));
                    FileWriter writer = new FileWriter(fileJSON);
                    StringBuilder result = new StringBuilder();
                    for (Person splitterPersonAfterDelete : personToDelete) {
                        result.append(toStringJSONForUpdate(splitterPersonAfterDelete)).append(",");
                    }
                    result = new StringBuilder(result.toString().replaceAll(",$", "]"));
                    if (result.toString().equals("[")) {
                        result = new StringBuilder();
                    }
                    writer.write(result.toString());
                    writer.close();
                    return SUCCESS_MESSAGE;
                } catch (IOException e) {
                    return FILE_WAS_NOT_FOUND;
                }

            case "lastName":
                try {
                    personToDelete.removeIf(person -> person.getLastName().equals(fieldValue));
                    FileWriter writer = new FileWriter(fileJSON);
                    StringBuilder result = new StringBuilder();
                    for (Person splitterPersonAfterDelete : personToDelete) {
                        result.append(toStringJSONForUpdate(splitterPersonAfterDelete)).append(",");
                    }
                    result = new StringBuilder(result.toString().replaceAll(",$", "]"));
                    if (result.toString().equals("[")) {
                        result = new StringBuilder();
                    }
                    writer.write(result.toString());
                    writer.close();
                    return SUCCESS_MESSAGE;
                } catch (IOException e) {
                    return FILE_WAS_NOT_FOUND;
                }

            case "age":
                try {
                    int age = Integer.parseInt(fieldValue);
                    personToDelete.removeIf(person -> person.getAge() == age);
                    FileWriter writer = new FileWriter(fileJSON);
                    StringBuilder result = new StringBuilder();
                    for (Person splitterPersonAfterDelete : personToDelete) {
                        result.append(toStringJSONForUpdate(splitterPersonAfterDelete)).append(",");
                    }
                    result = new StringBuilder(result.toString().replaceAll(",$", "]"));
                    if (result.toString().equals("[")) {
                        result = new StringBuilder();
                    }
                    writer.write(result.toString());
                    writer.close();
                    return SUCCESS_MESSAGE;
                } catch (NumberFormatException e) {
                    return WRONG_MESSAGE;
                } catch (IOException e) {
                    return FILE_WAS_NOT_FOUND;
                }

            case "city":
                try {
                    personToDelete.removeIf(person -> person.getCity().equals(fieldValue));
                    FileWriter writer = new FileWriter(fileJSON);
                    StringBuilder result = new StringBuilder();
                    for (Person splitterPersonAfterDelete : personToDelete) {
                        result.append(toStringJSONForUpdate(splitterPersonAfterDelete)).append(",");
                    }
                    result = new StringBuilder(result.toString().replaceAll(",$", "]"));
                    if (result.toString().equals("[")) {
                        result = new StringBuilder();
                    }
                    writer.write(result.toString());
                    writer.close();
                    return SUCCESS_MESSAGE;
                } catch (IOException e) {
                    return FILE_WAS_NOT_FOUND;
                }
            default:
                return "Enter value in ID-field!";
        }
    }

    public String deleteAll() {
        try {
            PrintWriter writer = new PrintWriter(fileJSON);
            writer.print("");
            writer.close();
        } catch (IOException e) {
            return "Error.";
        }
        return "All persons were deleted!";
    }

    public int createID() {
        List<Person> persons = readAll();
        try {
            return persons.get(persons.size() - 1).getId() + 1;
        } catch (Exception e) {
            return persons.size() + 1;
        }
    }

//    public String deletePerson(int id) {
//        ArrayList<Person> personToDelete = readFromJSON();
//        personToDelete.removeIf(person -> person.getId() == id);
//
//        try {
//            FileWriter writer = new FileWriter(fileJSON);
//
//            String result = "[";
//
//            for (Person splitterPersonAfterDelete : personToDelete) {
//                result += toStringJSON(splitterPersonAfterDelete) + ",";
//            }
//
//            result = result.replaceAll(",$", "]");
//            if (result == "[") {
//                result = "";
//            }
//
//            writer.write(result);
//            writer.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

//    public static String displayAll() {
//        ArrayList<Person> allPersons = readFromJSON();
//        StringBuilder displayPerson = new StringBuilder();
//        for (Person splitterPerson : allPersons) {
//            displayPerson.append(splitterPerson.toString());
//        }
//        return displayPerson.toString();
//
//    }


//    public static boolean checkId(int id) {
//        ArrayList<Person> personsList = readFromJSON();
//        for (Person splitterPerson : personsList) {
//            if (splitterPerson.getId() == id) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public static ObservableList<Person> converterStringToObservableList() {
//        return FXCollections.observableArrayList(readFromJSON());
//    }


    private String toStringJSON(Person person) {
        return "{" +
                "\"id\" : " + createID() +
                ", \"firstName\" : " + '\"' + person.getFirstName() + '\"' +
                ", \"lastName\" : " + '\"' + person.getLastName() + '\"' +
                ", \"age\" : " + person.getAge() +
                ", \"city\" : " + '\"' + person.getCity() + '\"' +
                '}';
    }

    private String toStringJSONForUpdate(Person person) {
        return "{" +
                "\"id\" : " + person.getId() +
                ", \"firstName\" : " + '\"' + person.getFirstName() + '\"' +
                ", \"lastName\" : " + '\"' + person.getLastName() + '\"' +
                ", \"age\" : " + person.getAge() +
                ", \"city\" : " + '\"' + person.getCity() + '\"' +
                '}';
    }
}
