package repository;

import dao.Person;
import javafx.collections.FXCollections;

import java.sql.*;
import java.util.List;

import static java.lang.Integer.parseInt;

public class MySQLRepository implements IRepository {

    static final String URL = "jdbc:mysql://eu-cdbr-west-03.cleardb.net:3306/heroku_1de9d50b9793109";
    static final String USER = "bfafa5984e369e";
    static final String PASSWORD = "245bc85f";


    private static Connection connection;
    private static Statement statement;
    private static PreparedStatement preparedStatement;
    private static ResultSet resultSet;

    public MySQLRepository() {

        try {
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            statement = connection.createStatement();
        } catch (SQLException sqlEx) {
            System.out.println("Connection Failed! Check output console");
            sqlEx.printStackTrace();
        }

        if (connection != null) {
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }

        try {
            String myTableName = "CREATE TABLE persons("
                    + "id   INT               NOT NULL AUTO_INCREMENT,"
                    + "fname VARCHAR (100)     NOT NULL,"
                    + "lname VARCHAR(100) NOT NULL,"
                    + "age  INT        NOT NULL,"
                    + "city   VARCHAR(100)  NOT NULL,"
                    + " PRIMARY KEY (id));";
            statement.executeUpdate(myTableName);
            System.out.println("Table created");
        } catch (SQLException e) {
        }
    }


    @Override
    public boolean createPerson(Person person) {
        try {
            String query = "INSERT INTO persons (fname,lname,age,city) VALUES (?,?,?,?)";

            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, person.getFirstName());
            preparedStatement.setString(2, person.getLastName());
            preparedStatement.setInt(3, person.getAge());
            preparedStatement.setString(4, person.getCity());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<Person> readAll() {

        List<Person> listOfPersons = FXCollections.observableArrayList();
        String query = "select id, fname, lname, age, city from persons";

        try {
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                int id = parseInt(resultSet.getString("id"));
                String fname = resultSet.getString("fname");
                String lname = resultSet.getString("lname");
                int age = parseInt(resultSet.getString("age"));
                String city = resultSet.getString("city");
                listOfPersons.add(new Person(id, fname, lname, age, city));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listOfPersons;
    }

    @Override
    public String updatePerson(int id, String fistName, String lastName, int age, String city) {

        String query = "UPDATE persons " +
                "SET fname = ?, lname = ?, age = ?, city = ? " +
                "WHERE id = ?;";
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, fistName);
            preparedStatement.setString(2, lastName);
            preparedStatement.setInt(3, age);
            preparedStatement.setString(4, city);
            preparedStatement.setInt(5, id);
            preparedStatement.executeUpdate();
            return "success";
        } catch (SQLException e) {
            e.printStackTrace();
            return "error";
        }
    }

    @Override
    public String deletePerson(String fieldName, String fieldValue) {
        String query = null;
        if (fieldName.equals("id")) {
            query = "DELETE FROM persons WHERE id=?";
        }
        if (fieldName.equals("firstName")) {
            query = "DELETE FROM persons WHERE fname=?";
        }
        if (fieldName.equals("lastName")) {
            query = "DELETE FROM persons WHERE lname=?";
        }
        if (fieldName.equals("age")) {
            query = "DELETE FROM persons WHERE age=?";
        }
        if (fieldName.equals("city")) {
            query = "DELETE FROM persons WHERE city=?";
        }
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, String.valueOf(fieldValue));
            preparedStatement.executeUpdate();
            return "success";
        } catch (SQLException e) {
            e.printStackTrace();
            return "error";
        }
    }

    @Override
    public String deleteAll() {
        try {
            String query = "TRUNCATE TABLE persons;";

            preparedStatement = connection.prepareStatement(query);

            preparedStatement.executeUpdate();

            return "success";

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return "error";
        }
    }
}
