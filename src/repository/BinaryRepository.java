package repository;


import dao.Person;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class BinaryRepository implements IRepository {

    public static File file = new File("persons.bin");
    private static final String FILE_WAS_NOT_FOUND = "File was not found!";

    public boolean createPerson(Person person) {

        try (DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(file, true))) {
            dataOutputStream.writeInt(createID());
            dataOutputStream.writeUTF(person.getFirstName());
            dataOutputStream.writeUTF(person.getLastName());
            dataOutputStream.writeInt(person.getAge());
            dataOutputStream.writeUTF(person.getCity());
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    public List<Person> readAll() {
        ArrayList<Person> listOfPersons = new ArrayList<>();
        try {
            DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(file, true));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            DataInputStream dataInputStream = new DataInputStream(new FileInputStream("persons.bin"));
            while (true) {
                Person person = new Person(dataInputStream.readInt(), dataInputStream.readUTF(),
                        dataInputStream.readUTF(), dataInputStream.readInt(), dataInputStream.readUTF());
                listOfPersons.add(person);
            }
        } catch (IOException e) {
            //e.printStackTrace();
        }
        return listOfPersons;
    }

    public String updatePerson(int id, String firstName, String lastName, int age, String city) {
        List<Person> personToUpdate = readAll();

        for (Person splitterPersonBeforeUpdate : personToUpdate) {
            if (splitterPersonBeforeUpdate.getId() == id) {
                splitterPersonBeforeUpdate.setFirstName(firstName);
                splitterPersonBeforeUpdate.setLastName(lastName);
                try {
                    splitterPersonBeforeUpdate.setAge(age);
                } catch (Exception ignored) {
                }
                splitterPersonBeforeUpdate.setCity(city);
            }
        }
        try {
            DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(file));

            for (Person person : personToUpdate) {
                createPerson(person);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Person was updated.";
    }

    public String deletePerson(String fieldName, String fieldValue) {
        final String WRONG_MESSAGE = "Wrong id was entered !";
        final String SUCCESS_MESSAGE = "Delete was successful!";
        List<Person> personToDelete = readAll();

        switch (fieldName) {
            case "id":
                try {
                    int id = Integer.parseInt(fieldValue);
                    personToDelete.removeIf(person -> person.getId() == id);
                    DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(file));
                    for (Person person : personToDelete) {
                        createPerson(person);
                    }
                    return SUCCESS_MESSAGE;
                } catch (NumberFormatException e) {
                    return WRONG_MESSAGE;
                } catch (FileNotFoundException e) {
                    return FILE_WAS_NOT_FOUND;
                }

            case "firstName":
                try {
                    DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(file));
                    personToDelete.removeIf(person -> person.getFirstName().equals(fieldValue));
                    for (Person person : personToDelete) {
                        createPerson(person);
                    }
                    return SUCCESS_MESSAGE;
                } catch (FileNotFoundException e) {
                    return FILE_WAS_NOT_FOUND;
                }

            case "lastName":
                try {
                    personToDelete.removeIf(person -> person.getLastName().equals(fieldValue));
                    DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(file));

                    for (Person person : personToDelete) {
                        createPerson(person);
                    }
                    return SUCCESS_MESSAGE;
                } catch (FileNotFoundException e) {
                    return FILE_WAS_NOT_FOUND;
                }

            case "age":
                try {
                    int age = Integer.parseInt(fieldValue);
                    personToDelete.removeIf(person -> person.getAge() == age);
                    DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(file));

                    for (Person person : personToDelete) {
                        createPerson(person);
                    }
                    return SUCCESS_MESSAGE;
                } catch (NumberFormatException e) {
                    return WRONG_MESSAGE;
                } catch (FileNotFoundException e) {
                    return FILE_WAS_NOT_FOUND;
                }

            case "city":
                try {
                    personToDelete.removeIf(person -> person.getCity().equals(fieldValue));
                    DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(file));

                    for (Person person : personToDelete) {
                        createPerson(person);
                    }
                    return SUCCESS_MESSAGE;
                } catch (FileNotFoundException e) {
                    return FILE_WAS_NOT_FOUND;
                }
            default:
                return "Enter value in ID-field!";
        }
    }

    public String deleteAll() {
        List<Person> listPersons = readAll();
        listPersons.clear();
        try {
            DataOutputStream dataOutputStream = new DataOutputStream((new FileOutputStream(file)));

        } catch (FileNotFoundException e) {
            return FILE_WAS_NOT_FOUND;
        }
        return "All persons were deleted.";
    }

    public int createID() {
        List<Person> persons;
        try {
            persons = readAll();
        } catch (Exception e) {
            return 0;
        }
        return persons.size() + 1;
    }

//    public boolean checkId(int id) {
//        ArrayList<Person> persons = readAll();
//        for (Person splitterPerson : persons) {
//            if (splitterPerson.getId() == id) {
//                return true;
//            }
//        }
//        return false;
//    }

    //    public String deletePerson(int id) {
//        ArrayList<Person> personToDelete = readAll();
//        personToDelete.removeIf(person -> person.getId() == id);
//
//        try {
//            DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(file));
//
//            for (int i = 0; i < personToDelete.size(); i++){
//                createPerson(personToDelete.get(i));
//            }
//            return "Person with this id - delete.";
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

//    public static String displayAll() {
//        ArrayList<Person> allPersons = readFromBinary();
//        StringBuilder displayPerson = new StringBuilder();
//        for (Person splitterPerson : allPersons) {
//            displayPerson.append(splitterPerson.toString());
//        }
//        return displayPerson.toString();
//
//    }
}
