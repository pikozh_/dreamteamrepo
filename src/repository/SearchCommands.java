package repository;

import dao.Person;

import java.util.List;
import java.util.stream.Collectors;

public class SearchCommands {
    public static List<Person> findPersonByID(int id, List<Person> persons) {
        List<Person> findPerson = persons.stream()
                .filter(person1 -> person1.getId() == id)
                .collect(Collectors.toList());
        return findPerson;
    }

    public static List<Person> findPersonByFirstName(String firstName, List<Person> persons) {
        List<Person> findPersons = persons.stream()
                .filter(person -> person.getFirstName().equals(firstName))
                .collect(Collectors.toList());
        return findPersons;
    }

    public static List<Person> findPersonByLastName(String lastName, List<Person> persons) {
        List<Person> findPersons = persons.stream()
                .filter(person -> person.getLastName().equals(lastName))
                .collect(Collectors.toList());
        return findPersons;
    }

    public static List<Person> findPersonByAge(int age, List<Person> persons) {
        List<Person> findPersons = persons.stream()
                .filter(person1 -> person1.getAge() == age)
                .collect(Collectors.toList());
        return findPersons;
    }

    public static List<Person> findPersonByCity(String city, List<Person> persons) {
        List<Person> findPersons = persons.stream()
                .filter(person -> person.getCity().equals(city))
                .collect(Collectors.toList());
        return findPersons;
    }
}
