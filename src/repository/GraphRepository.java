//package repository;
//
//import dao.Person;
//
//
//import java.util.List;
//
//import static org.neo4j.driver.Values.parameters;
//
//public class GraphRepository implements IRepository, AutoCloseable {
//    private final Driver driver;
//    private final String URI = "bolt://localhost:7687";
//    private final String USER = "neo4j";
//    private final String PASSWORD = "12345";
//
//    GraphDatabaseService graphDb;
//   ​
//    Node firstNode;
//    Node secondNode;
//    Relationship relationship;
//
//    public GraphRepository() {
//        driver = GraphDatabase.driver(URI, AuthTokens.basic(USER, PASSWORD));
//        System.out.println("Connection with NEO4J created");
//    }
//
//
//    @Override
//    public void close() throws Exception {
//        driver.close();
//    }
//
//    public void printGreeting(final String message) {
//        try (Session session = driver.session()) {
//            String greeting = session.writeTransaction(new TransactionWork<String>() {
//                @Override
//                public String execute(Transaction tx) {
//                    Result result = tx.run("CREATE (a:Greeting) " +
//                                    "SET a.message = $message " +
//                                    "RETURN a.message + ', from node ' + id(a)",
//                            parameters("message", message));
//                    return result.single().get(0).asString();
//                }
//            });
//            System.out.println(greeting);
//        }
//    }
//
//    public static void main(String... args) throws Exception {
//        try (GraphRepository greeter = new GraphRepository()) {
//            greeter.printGreeting("hello, world");
//        }
//    }
//
//    @Override
//    public boolean createPerson(Person person) {
//        String query = "CREATE (Ajeet:Developer{name: \"Ajeet Kumar\", YOB: 1989, POB: \"Mau\"}) ";
//
//        try ( Transaction tx = graphDb.beginTx() )
//       ​{
//           ​// Database operations go here
//           ​tx.commit();
//        }
//        }
//
//    @Override
//    public List<Person> readAll() {
//        return null;
//    }
//
//    @Override
//    public String updatePerson(int id, String fistName, String lastName, int age, String city) {
//        return null;
//    }
//
//    @Override
//    public String deletePerson(String fieldName, String fieldValue) {
//        return null;
//    }
//
//    @Override
//    public String deleteAll() {
//        return null;
//    }
//}
