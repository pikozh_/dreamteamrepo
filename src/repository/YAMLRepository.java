package repository;

import dao.Person;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class YAMLRepository implements IRepository {

    public static File file = new File("persons.yaml");

    public boolean createPerson(Person person) {
        try {
            FileWriter fileWriter = new FileWriter(file, true);
            List<Person> personList = Arrays.asList(person);
            for (Person splitterPersonList : personList) {
                fileWriter.write(writeYAMLConstructor(splitterPersonList));
            }
            fileWriter.close();
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    public List<Person> readAll() {

        List<Person> listOfPersons = new ArrayList<>();
        try {
            FileWriter fileWriter = new FileWriter(file, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            //BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String line = (Files.readAllLines(file.toPath())).toString();

            String[] personList = line.split(", ,   ");
            Arrays.stream(personList).takeWhile(splitterPerson -> !splitterPerson.equals("]"))
                    .takeWhile(splitterPerson -> !splitterPerson.equals("[]"))
                    .forEach(splitterPerson -> {
                        int id = Integer.parseInt(splitterPerson.substring(splitterPerson.indexOf("id") + 5,
                                splitterPerson.indexOf(",", splitterPerson.indexOf("id"))));

                        String firstName = splitterPerson.substring(splitterPerson.indexOf("first name") + 13,
                                splitterPerson.indexOf(",", splitterPerson.indexOf("first name")));

                        String lastName = splitterPerson.substring(splitterPerson.indexOf("last name") + 12,
                                splitterPerson.indexOf(",", splitterPerson.indexOf("last name")));

                        int age = Integer.parseInt(splitterPerson.substring(splitterPerson.indexOf("age") + 6,
                                splitterPerson.indexOf(",", splitterPerson.indexOf("age"))));

                        String city = splitterPerson.substring(splitterPerson.indexOf("city") + 7);
                        Person person = new Person(id, firstName, lastName, age, city);

                        listOfPersons.add(person);
                    });

        } catch (IOException e) {
            e.printStackTrace();
        }
        return listOfPersons;
    }

    public String updatePerson(int id, String firstName, String lastName, int age, String city) {
        List<Person> personToUpdate = readAll();

        for (Person splitterPersonBeforeUpdate : personToUpdate) {
            if (splitterPersonBeforeUpdate.getId() == id) {
                splitterPersonBeforeUpdate.setFirstName(firstName);
                splitterPersonBeforeUpdate.setLastName(lastName);
                splitterPersonBeforeUpdate.setAge(age);
                splitterPersonBeforeUpdate.setCity(city);
            }
        }
        try {
            FileWriter writer = new FileWriter(file);

            StringBuilder result = new StringBuilder();

            for (Person splitterPersonAfterUpdate : personToUpdate) {
                result.append(writeYAMLConstructorForUpdate(splitterPersonAfterUpdate));
            }
            result = new StringBuilder(result.toString().replaceAll(",", ""));
            writer.write(result.toString());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Person is updated.";
    }

    public String deletePerson(String fieldName, String fieldValue) {
        final String WRONG_MESSAGE = "Wrong entered id!";
        final String SUCCESS_MESSAGE = "Delete was success!";
        List<Person> personToDelete = readAll();

        switch (fieldName) {
            case "id":
                try {
                    int id = Integer.parseInt(fieldValue);
                    personToDelete.removeIf(person -> person.getId() == id);
                    FileWriter writer = new FileWriter(file);

                    StringBuilder result = new StringBuilder();

                    for (Person splitterPersonAfterDelete : personToDelete) {
                        result.append(writeYAMLConstructorForUpdate(splitterPersonAfterDelete));
                    }
                    writer.write(result.toString());
                    writer.close();
                    return SUCCESS_MESSAGE;
                } catch (NumberFormatException e) {
                    return WRONG_MESSAGE;
                } catch (IOException e) {
                    return "File not found!";
                }

            case "firstName":
                try {
                    personToDelete.removeIf(person -> person.getFirstName().equals(fieldValue));
                    FileWriter writer = new FileWriter(file);

                    StringBuilder result = new StringBuilder();

                    for (Person splitterPersonAfterDelete : personToDelete) {
                        result.append(writeYAMLConstructorForUpdate(splitterPersonAfterDelete));
                    }
                    writer.write(result.toString());
                    writer.close();
                    return SUCCESS_MESSAGE;
                } catch (IOException e) {
                    return "File not found!";
                }

            case "lastName":
                try {
                    personToDelete.removeIf(person -> person.getLastName().equals(fieldValue));
                    FileWriter writer = new FileWriter(file);

                    StringBuilder result = new StringBuilder();

                    for (Person splitterPersonAfterDelete : personToDelete) {
                        result.append(writeYAMLConstructorForUpdate(splitterPersonAfterDelete));
                    }
                    writer.write(result.toString());
                    writer.close();
                    return SUCCESS_MESSAGE;
                } catch (IOException e) {
                    return "File not found!";
                }

            case "age":
                try {
                    int age = Integer.parseInt(fieldValue);
                    personToDelete.removeIf(person -> person.getAge() == age);
                    FileWriter writer = new FileWriter(file);

                    StringBuilder result = new StringBuilder();

                    for (Person splitterPersonAfterDelete : personToDelete) {
                        result.append(writeYAMLConstructorForUpdate(splitterPersonAfterDelete));
                    }
                    writer.write(result.toString());
                    writer.close();
                    return SUCCESS_MESSAGE;
                } catch (NumberFormatException e) {
                    return WRONG_MESSAGE;
                } catch (IOException e) {
                    return "File not found!";
                }

            case "city":
                try {
                    personToDelete.removeIf(person -> person.getCity().equals(fieldValue));
                    FileWriter writer = new FileWriter(file);

                    String result = "";

                    for (Person splitterPersonAfterDelete : personToDelete) {
                        result += writeYAMLConstructorForUpdate(splitterPersonAfterDelete);
                        writer.write(result);
                    }
                    writer.close();
                    return SUCCESS_MESSAGE;
                } catch (IOException e) {
                    return "File not found!";
                }
            default:
                return "Enter value in ID-field!";
        }
    }

    public String deleteAll() {
        try {
            PrintWriter writer = new PrintWriter(file);
            writer.print("");
            writer.close();
        } catch (IOException e) {
            return "Error.";
        }
        return "All persons deleted.";
    }

//    public static void deletePerson(int id) {
//        ArrayList<Person> personToDelete = readFromYAML();
//        personToDelete.removeIf(person -> person.getId() == id);
//
//        try {
//            FileWriter writer = new FileWriter(file);
//
//            String result = "";
//
//            for (Person splitterPersonAfterDelete : personToDelete) {
//                result += writeYAMLConstructor(splitterPersonAfterDelete);
//            }
//
//            writer.write(result);
//            writer.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

//    public static String displayAll() {
//        ArrayList<Person> allPersons = readFromYAML();
//        StringBuilder displayPerson = new StringBuilder();
//        for (Person splitterPerson : allPersons) {
//            displayPerson.append(splitterPerson.toString());
//        }
//        return displayPerson.toString();
//
//    }

    public int createID() {
        List<Person> persons = readAll();
        try {
            return persons.get(persons.size() - 1).getId() + 1;
        } catch (Exception e) {
            return persons.size() + 1;
        }
    }

//    public static boolean checkId(int id) {
//        ArrayList<Person> persons = readFromYAML();
//        for (Person splitterPerson : persons) {
//            if (splitterPerson.getId() == id) {
//                return true;
//            }
//        }
//        return false;
//    }

    public String writeYAMLConstructor(Person person) {

        return "persona" + createID() + ":"
                + "\n\t - id : " + createID()
                + "\n\t - first name : " + person.getFirstName()
                + "\n\t - last name : " + person.getLastName()
                + "\n\t - age : " + person.getAge()
                + "\n\t - city : " + person.getCity() + "\n\n  ";
    }

    public String writeYAMLConstructorForUpdate(Person person) {

        return "persona" + person.getId() + ":"
                + "\n\t - id : " + person.getId()
                + "\n\t - first name : " + person.getFirstName()
                + "\n\t - last name : " + person.getLastName()
                + "\n\t - age : " + person.getAge()
                + "\n\t - city : " + person.getCity() + "\n\n  ";
    }

//    public static ObservableList<Person> converterStringToObservableList(){
//        return FXCollections.observableArrayList(readFromYAML());
//    }
}
