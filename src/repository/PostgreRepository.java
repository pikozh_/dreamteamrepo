package repository;

import dao.Person;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
import java.util.List;

import static java.lang.Integer.parseInt;

public class PostgreRepository implements IRepository {

    private static Statement stmt;
    private static PreparedStatement preparedStatement;
    private static ResultSet rs;
    private static Connection connection = null;

    public PostgreRepository() {
        System.out.println("-------- PostgreSQL "
                + "JDBC Connection Testing ------------");

        try {

            Class.forName("org.postgresql.Driver");

        } catch (ClassNotFoundException e) {

            System.out.println("Where is your PostgreSQL JDBC Driver? "
                    + "Include in your library path!");
            e.printStackTrace();


        }
        System.out.println("PostgreSQL JDBC Driver Registered!");


        try {
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://ec2-3-248-4-172.eu-west-1.compute.amazonaws.com:5432/d53knvbfufupa3", "gnqzinvidwoiqa",
                    "3b966ca1a99d7a2f0e97131be7bd08862b343ef02467ced66ad98e855076db34");
            stmt = connection.createStatement();
        } catch (SQLException e) {

            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();


        }

        if (connection != null) {
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }


        try {


            String myTableName = "CREATE TABLE persons("
                    + "id   SERIAL PRIMARY KEY,"
                    + "fname CHARACTER VARYING(30)     NOT NULL,"
                    + "lname CHARACTER VARYING(30) NOT NULL,"
                    + "age  INTEGER           NOT NULL,"
                    + "city  CHARACTER VARYING(30)  NOT NULL);";
            stmt.executeUpdate(myTableName);
            System.out.println("Table created");
        } catch (SQLException throwables) {

        }

    }


    @Override
    public boolean createPerson(Person person) {
        try {
            String query = "INSERT INTO persons (fname,lname,age,city) VALUES (? ,?,?,?)";

            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, person.getFirstName());
            preparedStatement.setString(2, person.getLastName());
            preparedStatement.setInt(3, person.getAge());
            preparedStatement.setString(4, person.getCity());
            preparedStatement.executeUpdate();

            return true;

        } catch (SQLException throwables) {

            return false;
        }
    }

    @Override
    public List<Person> readAll() {
        try {
            ObservableList<Person> listOfPersons = FXCollections.observableArrayList();
            String query = "select id, fname, lname, age, city from persons";

            rs = stmt.executeQuery(query);
            while (rs.next()) {
                int id = parseInt(rs.getString("id"));
                String fname = rs.getString("fname");
                String lname = rs.getString("lname");
                int age = parseInt(rs.getString("age"));
                String city = rs.getString("city");


                listOfPersons.add(new Person(id, fname, lname, age, city));
            }
            String j = "6";
            return listOfPersons;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }

    }

    @Override
    public String updatePerson(int id, String fistName, String lastName, int age, String city) {

        try {
            String query = "UPDATE persons " +
                    "SET fname = ?, lname = ?, age = ?, city = ? " +
                    "WHERE id = ?;";

            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, fistName);
            preparedStatement.setString(2, lastName);
            preparedStatement.setInt(3, age);
            preparedStatement.setString(4, city);
            preparedStatement.setInt(5, id);

            preparedStatement.executeUpdate();
            return "success";

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return "error";
        }

    }

    @Override
    public String deletePerson(String fieldName, String fieldValue) {
        try {
            String query = null;

            if (fieldName.equals("firstName")) {
                query = "DELETE FROM persons WHERE fname=?";

            }
            if (fieldName.equals("lastName")) {
                query = "DELETE FROM persons WHERE lname=?";

            }

            if (fieldName.equals("city")) {
                query = "DELETE FROM persons WHERE city=?";

            }
            if (fieldName.equals("age")) {
                query = "DELETE FROM persons WHERE age=?";
                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setInt(1, Integer.parseInt(fieldValue));
                preparedStatement.executeUpdate();

            }  else if  (fieldName.equals("id")) {
                query = "DELETE FROM persons WHERE id=?";
                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setInt(1, Integer.parseInt(fieldValue));
                preparedStatement.executeUpdate();
            }
            else {
                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, String.valueOf(fieldValue));
                preparedStatement.executeUpdate();
            }

            return "success";
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return "error";
        }

    }

    @Override
    public String deleteAll() {
        try {

            String query = "TRUNCATE TABLE persons;";

            preparedStatement = connection.prepareStatement(query);
            preparedStatement.executeUpdate();
            return "success";
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return "error";
        }

    }
}

