package repository;

import dao.Person;
import utils.Constants;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class H2Repository implements IRepository {

    @Override
    public boolean createPerson(Person person) {
        person.setId(createId());
        try {
            var result = new H2Context(false).Connect()
                    .createStatement()
                    .execute(Constants.INSERT + person.toQueryString());
            return true;
        } catch (SQLException | ClassNotFoundException e) {
            return false;
        }
    }

    @Override
    public List<Person> readAll() {
        try {
            var something = new H2Context(false).Connect()
                    .createStatement()
                    .executeQuery(Constants.GET_ALL_PERSONS);
            var persons = TransformToListPerson(something);
            System.out.println(Constants.SUCCESS);

            return persons;
        } catch (Exception e) {
            System.out.println(Constants.ERROR);
            e.printStackTrace();
            return new ArrayList<Person>();
        }
    }

    @Override
    public String updatePerson(int id, String firstName, String lastName, int age, String city) {
        var person = new Person(id, firstName, lastName, age, city);
        try {
            var something = new H2Context(false).Connect()
                    .createStatement()
                    .execute(Constants.UPDATE_TABLE_PERSON + person.toDeleteQueryString() +
                            Constants.WHERE_STATEMENT + person.getId());
            System.out.println(Constants.SUCCESS);

            return Constants.SUCCESS_UPDATE;
        } catch (Exception e) {
            System.out.println(Constants.ERROR);
            e.printStackTrace();
            return Constants.ERROR;
        }
    }

    @Override
    public String deletePerson(String fieldName, String fieldValue) {
        switch (fieldName) {
            case "id":
                int id;
                try {
                    id = Integer.parseInt(fieldValue);
                    new H2Context(false).Connect()
                            .createStatement()
                            .execute(Constants.DELETE + id);
                    return Constants.SUCCESS_DELETE;
                } catch (SQLException | ClassNotFoundException e) {
                    return Constants.ERROR;
                }

            case "firstName":
                try {
                    new H2Context(false).Connect()
                            .createStatement()
                            .execute(Constants.DELETE_ALL_BY_FIRST_NAME + "'" + fieldValue + "'");
                    return Constants.SUCCESS_DELETE;
                } catch (SQLException | ClassNotFoundException e) {
                    return Constants.ERROR;
                }

            case "lastName":
                try {
                    new H2Context(false).Connect()
                            .createStatement()
                            .execute(Constants.DELETE_ALL_BY_LAST_NAME + "'" + fieldValue + "'");
                    return Constants.SUCCESS_DELETE;
                } catch (SQLException | ClassNotFoundException e) {
                    return Constants.SUCCESS_DELETE;
                }

            case "age":
                int age;
                try {
                    age = Integer.parseInt(fieldValue);
                    new H2Context(false).Connect()
                            .createStatement()
                            .execute(Constants.DELETE_ALL_BY_AGE + age);
                    return Constants.SUCCESS_DELETE;
                } catch (NumberFormatException | SQLException | ClassNotFoundException e) {
                    return Constants.ERROR;
                }

            case "city":
                try {
                    new H2Context(false).Connect()
                            .createStatement()
                            .execute(Constants.DELETE_ALL_BY_CITY + "'" + fieldValue + "'");
                    return Constants.SUCCESS_DELETE;
                } catch (SQLException | ClassNotFoundException e) {
                    return Constants.ERROR;
                }
                default:
                    return "Success";
        }
    }

    @Override
    public String deleteAll(){
        try {
            new H2Context(false).Connect()
                    .createStatement()
                    .execute(Constants.DELETE_ALL_PERSONS);
            return Constants.SUCCESS_DELETE;
        }catch (SQLException | ClassNotFoundException e ){
            return Constants.ERROR;
        }

    }

    private static ArrayList<Person> TransformToListPerson(ResultSet resultSet) throws SQLException {
        var list = new ArrayList<Person>();
        while (resultSet.next()) {
            var person = new Person();
            person.setId(resultSet.getInt("id"));
            person.setFirstName(resultSet.getString("firstName"));
            person.setLastName(resultSet.getString("lastName"));
            person.setAge(resultSet.getInt("age"));
            person.setCity(resultSet.getString("city"));
            list.add(person);
        }

        return list;
    }

    private int createId() {
        List<Person> persons = readAll();
        try {
            return persons.get(persons.size() - 1).getId() + 1;
        } catch (Exception e){
            return persons.size() + 1;
        }
    }
}
