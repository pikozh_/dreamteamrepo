
import dao.Person;
import org.junit.jupiter.api.Test;
import repository.YAMLRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class YAMLRepositoryTest {

    YAMLRepository yamlRepository = new YAMLRepository();

    @Test
    void createPerson_ShouldCreateNewPerson() {

        //given
        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");

        //when
        boolean actual = yamlRepository.createPerson(person);

        //then
        assertTrue(actual);
    }

    @Test
    void readAll_ShouldReadFile() {

        //given
        List<Person> expected = new ArrayList();

        //when
        String prepareTheFile = yamlRepository.deleteAll();
        List<Person> actual = yamlRepository.readAll();

        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        boolean prepareTheFileAgain = yamlRepository.createPerson(person);
        List<Person> actual1 = yamlRepository.readAll();

        //then
        assertEquals(expected, actual);
        assertNotEquals(expected, actual1);
    }

    @Test
    void updatePerson_ShouldUpdatePerson() {

        //given
        String expected = "The changes were successful!";

        //when
        String actual = yamlRepository.updatePerson(1, "Dariia", "Kaptiurova", 20, "Kyiv");

        //then
        assertEquals(expected, actual);
    }

    @Test
    void deletePerson_ShouldDeletePerson() {

        //given
        String fieldName = "id";
        String fieldValue = "1";

        String fieldName1 = "firstName";
        String fieldValue1 = "Dariia";

        String fieldName2 = "lastName";
        String fieldValue2 = "Kaptiurova";

        String fieldName3 = "age";
        String fieldValue3 = "20";

        String fieldName4 = "city";
        String fieldValue4 = "Kyiv";

        String fieldName5 = "id";
        String fieldValue5 = "90";

        //when
        String expected = "Delete was successful!";
        String actual = yamlRepository.deletePerson(fieldName, fieldValue);

        String expected1 = "Delete was successful!";
        String actual1 = yamlRepository.deletePerson(fieldName1, fieldValue1);

        String expected2 = "Delete was successful!";
        String actual2 = yamlRepository.deletePerson(fieldName2, fieldValue2);

        String expected3 = "Delete was successful!";
        String actual3 = yamlRepository.deletePerson(fieldName3, fieldValue3);

        String expected4 = "Delete was successful!";
        String actual4 = yamlRepository.deletePerson(fieldName4, fieldValue4);

        String expected5 = "Enter value in ID-field!";
        String actual5 = yamlRepository.deletePerson(fieldName5, fieldValue5);

        //then
        assertEquals(expected, actual);
        assertEquals(expected1, actual1);
        assertEquals(expected2, actual2);
        assertEquals(expected3, actual3);
        assertEquals(expected4, actual4);
        assertNotEquals(expected5, actual5);
    }

    @Test
    void deleteAll_ShouldDeleteAllPeople() {

        //given

        //when
        String expected = "All persons were deleted.";
        String actual = yamlRepository.deleteAll();

        //then
        assertEquals(expected, actual);
    }

    @Test
    void createID_ShouldCreateID() {

        //given
        String prepareTheFile = yamlRepository.deleteAll();
        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        yamlRepository.createPerson(person);

        //when
        int expected = 2;
        int actual = yamlRepository.createID();

        int expected1 = 0;
        int actual1 = yamlRepository.createID();

        //then
        assertEquals(expected, actual);
        assertNotEquals(expected1, actual1);
    }

    @Test
    void writeYAMLConstructor() {

        //given
        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        String expected = "persona" + 1 + ":"
                + "\n\t - id : " + 1
                + "\n\t - first name : " + "Dariia"
                + "\n\t - last name : " + "Kaptiurova"
                + "\n\t - age : " + 20
                + "\n\t - city : " + "Kyiv" + "\n\n  ";
        Person person1 = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        String expected1 = "Dariia, Kaptiurova";

        //when
        String actual = yamlRepository.writeYAMLConstructor(person);
        String actual1 = yamlRepository.writeYAMLConstructor(person);

        //then
        assertEquals(expected, actual);
        assertNotEquals(expected1, actual1);
    }

    @Test
    void writeYAMLConstructorForUpdate() {

        //given
        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        String expected = "persona" + 0 + ":"
                + "\n\t - id : " + 0
                + "\n\t - first name : " + "Dariia"
                + "\n\t - last name : " + "Kaptiurova"
                + "\n\t - age : " + 20
                + "\n\t - city : " + "Kyiv" + "\n\n  ";
        Person person1 = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        String expected1 = "Dariia, Kaptiurova";

        //when
        String actual = yamlRepository.writeYAMLConstructorForUpdate(person);
        String actual1 = yamlRepository.writeYAMLConstructorForUpdate(person1);

        //then
        assertEquals(expected, actual);
        assertNotEquals(expected1, actual1);
    }
}