
import dao.Person;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.Test;
import repository.PostgreRepository;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PostgreSQLRepositoryTest {

    PostgreRepository postgreRepository = new PostgreRepository();

    @Test
    public void createPersonTest() {

        //given
        Person person = new Person(1, "Dariia", "Kaptiurova", 20, "Kyiv");

        //when
        boolean actual = postgreRepository.createPerson(person);

        //then
        assertTrue(actual);
    }

    @Test
    public void updatePersonTest() {

        //given
        String expected = "The changes were successful!";

        //when
        String actual = postgreRepository.updatePerson(1, "Dariia", "Kaptiurova", 20, "Kyiv");

        //then
        assertEquals(expected, actual);
    }


    @Test
    public void deleteALLPersonTest() {

        //given
        String expected = "Persons were deleted!";

        //when
        String actual = postgreRepository.deleteAll();

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void deleteALLPersonFieldTest() {

        //given
        String expected = "Persons were deleted!";

        //when
        String actual = postgreRepository.deletePerson("id", "1");

        //then
        assertEquals(expected, actual);
    }


    @Test
    public void readAllFailedTest() {

        List<Person> expected = FXCollections.observableArrayList();
        expected.add(new Person(11, "1", "2", 3, "4"));

        //when
        List<Person> actual = postgreRepository.readAll();

        //then
        assertNotEquals(expected.toString(), actual.toString());
    }
}