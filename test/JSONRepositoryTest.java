
import dao.Person;
import org.junit.jupiter.api.Test;
import repository.JSONRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class JSONRepositoryTest {

    JSONRepository jsonRepository = new JSONRepository();

    @Test
    void createPerson_ShouldCreateNewPerson() {

        //given
        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");

        //when
        boolean actual = jsonRepository.createPerson(person);

        //then
        assertTrue(actual);
    }

    @Test
    void readAll_ShouldReadFile() {

        //given
        List<Person> expected = new ArrayList();

        //when
        String prepareTheFile = jsonRepository.deleteAll();
        List<Person> actual = jsonRepository.readAll();

        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        boolean prepareTheFileAgain = jsonRepository.createPerson(person);
        List<Person> actual1 = jsonRepository.readAll();

        //then
        assertEquals(expected, actual);
        assertNotEquals(expected, actual1);
    }

    @Test
    void updatePerson_ShouldUpdatePerson() {

        //given
        String expected = "The changes were successful!";

        //when
        String actual = jsonRepository.updatePerson(1, "Dariia", "Kaptiurova", 20, "Kyiv");

        //then
        assertEquals(expected, actual);
    }

    @Test
    void deletePerson_ShouldDeletePerson() {

        //given
        String fieldName = "id";
        String fieldValue = "1";

        String fieldName1 = "firstName";
        String fieldValue1 = "Dariia";

        String fieldName2 = "lastName";
        String fieldValue2 = "Kaptiurova";

        String fieldName3 = "age";
        String fieldValue3 = "20";

        String fieldName4 = "city";
        String fieldValue4 = "Kyiv";

        String fieldName5 = "id";
        String fieldValue5 = "90";

        //when
        String expected = "Delete was successful!";
        String actual = jsonRepository.deletePerson(fieldName, fieldValue);

        String expected1 = "Delete was successful!";
        String actual1 = jsonRepository.deletePerson(fieldName1, fieldValue1);

        String expected2 = "Delete was successful!";
        String actual2 = jsonRepository.deletePerson(fieldName2, fieldValue2);

        String expected3 = "Delete was successful!";
        String actual3 = jsonRepository.deletePerson(fieldName3, fieldValue3);

        String expected4 = "Delete was successful!";
        String actual4 = jsonRepository.deletePerson(fieldName4, fieldValue4);

        String expected5 = "Enter value in ID-field!";
        String actual5 = jsonRepository.deletePerson(fieldName5, fieldValue5);

        //then
        assertEquals(expected, actual);
        assertEquals(expected1, actual1);
        assertEquals(expected2, actual2);
        assertEquals(expected3, actual3);
        assertEquals(expected4, actual4);
        assertNotEquals(expected5, actual5);
    }

    @Test
    void deleteAll_ShouldDeleteAllPeople() {

        //given

        //when
        String expected = "All persons were deleted!";
        String actual = jsonRepository.deleteAll();

        //then
        assertEquals(expected, actual);
    }

    @Test
    void createID_ShouldCreateID() {

        //given
        String prepareTheFile = jsonRepository.deleteAll();
        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        jsonRepository.createPerson(person);

        //when
        int expected = 2;
        int actual = jsonRepository.createID();

        int expected1 = 0;
        int actual1 = jsonRepository.createID();

        //then
        assertEquals(expected, actual);
        assertNotEquals(expected1, actual1);
    }
}